var tkn = pjs.require("validateToken.js");
function testToken(){

var token;
token = '2022020413170048d55ad0b495d3f5e2be10fb433b6340acc3ef9e67b7fb92e3b1343a81fa3f27';  // valid for one year

//token = '2021010315470076233e565753c64c1017694fcc342d2e8aadc80318e4ee577a4fc8af8111fcf8';  // expired

//token = '20200104170000c614ab5a8661061ff9b8d7cf867decdf257fea64fecf1f82fe443aeb23d7f563';  // invalid

//token = '20200104170000c614';                                                              // corrupt

token = '202101061725006920d3a57779f451b6a8b0c2099943e47f54a811c328cff5f946b74fbdbd811d';

var validate = {};
validate = tkn.validateToken(token);

console.log("token valid: " + validate.tokenIsValid);
console.log("message: " + validate.message);

}
exports.run = testToken;