﻿CREATE TABLE TYLER.TEMPBULTIN (
	T_ITEMCODE CHAR(5) CCSID 37 NOT NULL DEFAULT '' ,
	T_CHKDIGIT CHAR(1) CCSID 37 NOT NULL DEFAULT '' ,
	T_PACKSIZE CHAR(10) CCSID 37 NOT NULL DEFAULT '' ,
	T_DESCRIPT CHAR(50) CCSID 37 NOT NULL DEFAULT '' ,
	T_DEPT CHAR(1) CCSID 37 NOT NULL DEFAULT '' ,
	T_BOH DECIMAL(5, 0) NOT NULL DEFAULT 0 ,
    T_COST DECIMAL(5, 2) NOT NULL DEFAULT 0 ,
    T_UNITCOST DECIMAL(5, 2) NOT NULL DEFAULT 0 ,
    T_SRP DECIMAL(5, 2) NOT NULL DEFAULT 0 ,
    T_SHIPDATE CHAR(25) CCSID 37 NOT NULL DEFAULT '' ,
    T_SHIPQTY DECIMAL(7, 0) NOT NULL DEFAULT 0 ,
	T_SHOWDOLL DECIMAL(4, 2) NOT NULL DEFAULT 0 ,
	
	PRIMARY KEY( T_ITEMCODE, T_SHIPDATE ) )

	RCDFMT TEMPBULTIN ;

LABEL ON TABLE TYLER.TEMPBULTIN 
	IS 'Temporary bulletin Table' ;

LABEL ON COLUMN TYLER.TEMPBULTIN 
(
    T_ITEMCODE TEXT IS 'Item Code' ,
	T_CHKDIGIT TEXT IS 'Check Digit' ,
	T_PACKSIZE TEXT IS 'Pack Size' ,
	T_DESCRIPT TEXT IS 'Description' ,
	T_DEPT TEXT IS 'Department' ,
	T_BOH TEXT IS 'BoH' ,
    T_COST TEXT IS 'Cost' ,
    T_UNITCOST TEXT IS 'Unit cost' ,
    T_SRP TEXT IS 'SRP' ,
    T_SHIPDATE TEXT IS 'Store Ship Date' ,
    T_SHIPQTY TEXT IS 'Store Ship Quantity' ,
	T_SHOWDOLL TEXT IS 'Store Show Dollars' 
);

select * from tyler.TEMPBULTIN;

