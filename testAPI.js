//var tkn = pjs.require("validateToken.js");
function getorder(request, response) {
    var method =  request.method
 
// Token Validation    
    //var token = request.headers.token
    //var validate = {};
    //    validate = tkn.validateToken(token);
    //    console.log("token valid: " + validate.tokenIsValid);
    //    console.log("message: " + validate.message);
//if (!validate.tokenIsValid) response.status(400).send(validate.message);

    if (method == "POST")
        goInsert();
    else if (method == "GET") 
        goSelect();
    else if (method == "PUT")
        goSelect();
    else
        goBadMethod();

//------------------------------------------------------------------------------------------------------
function  goSelect(){
    var storeNumber = request.params.id;
    console.log(storeNumber);

    var recordSet;
    var sqlquery;
    if (storeNumber){

        sqlquery = "select * from tyler.bultin where bstore = ?" //I need to pass multiple paramenters to query with (store number, item number, date, etc.)
        recordSet = pjs.query(sqlquery, [storeNumber]);
    
        if(recordSet)
        {
        // response.json({                                                      
        //   storeNumber: recordSet[0]["bflag"],
        //   City: recordSet[0]["bstore"],
        //   ZipCode: recordSet[0]["bitem"],
        //   storeAddress: recordSet[0]["bqty"],
        //   });
        response.json(recordSet);
        console.log(recordSet);
        }
        else
        {
            response.status(400).send("No Record Found for Store Number " + storeNumber);
        }
    }
    else{
      response.status(400).send("Id not received");
    }
    }
//-------------------------------------------------------------------------------------------------------

function goInsert(){
    var recordSet;
    var sqlquery;
    var errorMsg;
    //var parmListArray = [];
    
    if (!request.body.storeNumber || !request.body.itemNumber)
    {
      errorMsg = "storeNumber and itemNumber are required ";
      response.status(400).send(errorMsg);
    }
    else
    {
    //   parmListArray.push(request.body.storeNumber);
    //   parmListArray.push(request.body.itemNumber);
    //   console.log(parmListArray);
      
      storeNumber = request.body.storeNumber;
      itemNumber = request.body.itemNumber;

      console.log(storeNumber + " & " + itemNumber);
      
      sqlquery = "select * from tyler.bultin where bstore = ? and bitem = ?" //I need to pass multiple paramenters to query with (store number, item number, date, etc.)
        recordSet = pjs.query(sqlquery, [storeNumber, itemNumber]);
    
        if(recordSet)
        {
            //console.log(recordSet);
            response.json(recordSet);
        }
        else
        {
            response.status(400).send("No Record Found for Store Number " + storeNumber);
        }
    }
    
  }
//-----------------------------------------------------------------------------------------------------------
/*function goInsert(){
    var recordSet;
    var sqlquery;
    var errorMsg;
    
    if (!request.storeNumber)
    {
      errorMsg = "storeNumber and itemNumber are required ";
      response.status(400).send(errorMsg);
    }
    else
    {
      
      storeNumber = request.storeNumber; //Would I need to set an array and loop through it to apply each storeNumber in the array to a sqlquery?
      console.log(storeNumber);
      
      sqlquery = "select * from tyler.bultin where bstore = ?"
        recordSet = pjs.query(sqlquery, [storeNumber]);
    
        if(recordSet)
        {
            //console.log(recordSet);
            response.json(recordSet);
        }
        else
        {
            response.status(400).send("No Record Found for Store Number " + storeNumber);
        }
    }
    
  } */
//-----------------------------------------------------------------------------------------------------------
function goBadMethod(){
    var  error ="no method received";
    response.status(400).send(error);
  }

}      
exports.run = getorder;